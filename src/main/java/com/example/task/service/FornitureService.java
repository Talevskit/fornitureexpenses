package com.example.task.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.task.entity.Forniture;
import com.example.task.repo.FornitureRepo;

@Service
public class FornitureService implements FornitureRepo{
	

	Forniture forniture = new Forniture();


	public int calculateWood(int woodCount) {
		return woodCount * 9;
	}

	public int calculateGlass(int glassCount) {
		return glassCount * 11;
	}

	public int calculatePlastic(int plasticCount) {
		return plasticCount * 4;
	}

	public int calculateFornitureCost(Forniture forniture) throws Exception {
		switch (forniture.getType()) {
		case "chair":
			int chairCost = 0;
			if (forniture.getWood() != null) {
				chairCost += calculateWood(forniture.getWood());
			}
			if (forniture.getGlass() != null) {
				chairCost += calculateGlass(forniture.getGlass());
			}
			return chairCost;

		case "cupboard":

			int cupboardCost = 0;
			if (forniture.getWood() != null) {
				cupboardCost += calculateWood(forniture.getWood());
			}
			return cupboardCost;

		case "table":
			int plasticCost = 0;
			if (forniture.getWood() != null) {
				plasticCost += calculateWood(forniture.getWood());
			}
			if (forniture.getGlass() != null) {
				plasticCost += calculateGlass(forniture.getGlass());
			}
			if (forniture.getPlastic() != null) {
				plasticCost += calculatePlastic(forniture.getPlastic());
			}
			return plasticCost;
			default:
				throw new Exception("Wrong type");
		}
	}

	@Override
	public Forniture getMostExpensiveForniture(List<Forniture> fornitures) throws Exception {
		int mostExpensiveCost = 0;
		Forniture mostExpensiveForniture = null;

		for (Forniture forniture : fornitures) {
			int fornitureCost = calculateFornitureCost(forniture);
			if (fornitureCost > mostExpensiveCost) {
				mostExpensiveCost = fornitureCost;
				mostExpensiveForniture = forniture;
			}
		}
		return mostExpensiveForniture;
	}
}
