package com.example.task.entity;

import lombok.Data;

@Data
public class Forniture {

	private String type;
	private Integer wood;
	private Integer glass;
	private Integer plastic;
	
}
