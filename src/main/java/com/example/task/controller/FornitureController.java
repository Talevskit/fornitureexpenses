	package com.example.task.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.task.entity.Forniture;
import com.example.task.service.FornitureService;

@RequestMapping("/forniture")
@Controller
public class FornitureController {
	
	@Autowired
	private FornitureService fornitureService;
	
	@PostMapping("/theMostExpensive")
	public Forniture getMostExpensiveForniture(@RequestBody List<Forniture> forniture) throws Exception{
		return fornitureService.getMostExpensiveForniture(forniture);
	}
	
	

}
