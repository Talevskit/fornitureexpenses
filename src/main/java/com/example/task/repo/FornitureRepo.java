package com.example.task.repo;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.example.task.entity.Forniture;

@Repository
public interface FornitureRepo {
	
	public Forniture getMostExpensiveForniture(List<Forniture> forniture) throws Exception;

}
